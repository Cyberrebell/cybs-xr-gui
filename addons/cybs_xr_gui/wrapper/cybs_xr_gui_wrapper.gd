extends Sprite3D

class_name CybsXRGUIWrapper

@onready var viewport : SubViewport = find_children("", "SubViewport", false).front()
@onready var sprite_size : Vector2

func _ready():
	var shape = $StaticBody3D/CollisionShape3D.shape
	sprite_size = pixel_size * viewport.get_size()
	shape.size.x = sprite_size.x
	shape.size.y = sprite_size.y

func click_push_at(hit_position : Vector3) -> void:
	var event = InputEventMouseButton.new()
	event.button_index = MOUSE_BUTTON_LEFT
	event.position = _calculate_2d_position(hit_position)
	event.pressed = true
	viewport.push_input(event)

func click_release_at(hit_position : Vector3) -> void:
	var event = InputEventMouseButton.new()
	event.button_index = MOUSE_BUTTON_LEFT
	event.position = _calculate_2d_position(hit_position)
	event.pressed = false
	viewport.push_input(event)

func move_to(hit_position : Vector3) -> void:
	var event = InputEventMouseMotion.new()
	event.position = _calculate_2d_position(hit_position)
	viewport.push_input(event)

func set_active(active : bool) -> void:
	visible = active
	$StaticBody3D/CollisionShape3D.disabled = !active

func _calculate_2d_position(hit_position : Vector3) -> Vector2:
	return Vector2(hit_position.x + sprite_size.x / 2, sprite_size.y / 2 - hit_position.y) / pixel_size
