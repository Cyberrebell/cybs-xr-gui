extends RayCast3D

class_name CybsXRGUIPointer

signal clicked_out

var click_is_active : bool = false

func click_push():
	if is_colliding():
		var wrapper : CybsXRGUIWrapper = get_collider().get_parent()
		wrapper.click_push_at(wrapper.to_local(get_collision_point()))
		click_is_active = true
	else:
		clicked_out.emit()

func click_release():
	if is_colliding():
		var wrapper : CybsXRGUIWrapper = get_collider().get_parent()
		wrapper.click_release_at(wrapper.to_local(get_collision_point()))
		click_is_active = false

func _physics_process(delta):
	if click_is_active:
		if is_colliding():
			var wrapper : CybsXRGUIWrapper = get_collider().get_parent()
			wrapper.move_to(wrapper.to_local(get_collision_point()))
