extends MeshInstance3D

@onready var pointer : CybsXRGUIPointer = get_parent()
@onready var max_target_z : float = pointer.target_position.z
@onready var hit_marker : MeshInstance3D = $hit_marker

func _ready():
	$hit_marker.set_surface_override_material(0, get_surface_override_material(0))

func _physics_process(delta):
	var target_z : float = position.z + to_local(pointer.get_collision_point()).z  if pointer.is_colliding() else max_target_z
	mesh.size.z = abs(target_z)
	position.z = target_z / 2
	hit_marker.position.z = target_z / 2
