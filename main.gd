extends Node3D

func _ready():
	get_viewport().use_xr = true

func toggle_static_menu() -> void:
	$menu.set_active(!$menu.visible)

func toggle_visor_menu() -> void:
	$XROrigin3D/XRCamera3D/menu.set_active(!$XROrigin3D/XRCamera3D/menu.visible)

func close_all_menus() -> void:
	$menu.set_active(false)
	$XROrigin3D/XRCamera3D/menu.set_active(false)

func should_show_pointer() -> bool:
	return $menu.visible || $XROrigin3D/XRCamera3D/menu.visible

func _on_cybs_xrgui_pointer_clicked_out():
	close_all_menus()
