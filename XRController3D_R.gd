extends XRController3D

@onready var pointer : CybsXRGUIPointer = $CybsXRGUIPointer
@onready var main = get_parent().get_parent()

func _on_button_pressed(button_name : String):
	match button_name:
		"trigger_click":
			pointer.click_push()
		"ax_button":
			main.toggle_static_menu()
			$CybsXRGUIPointer.visible = main.should_show_pointer()
		"by_button":
			main.toggle_visor_menu()
			$CybsXRGUIPointer.visible = main.should_show_pointer()

func _on_button_released(button_name : String):
	match button_name:
		"trigger_click":
			pointer.click_release()
