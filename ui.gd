extends Control

func _on_button_pressed():
	$Button/Label.text += "B"

func _on_check_button_toggled(button_pressed : bool):
	var viewport : SubViewport = get_parent()
	viewport.transparent_bg = button_pressed

func _on_h_slider_value_changed(value : float):
	$HSlider/Label.text = "Volume: " + str(value)
